var express = require('express');
var router = express.Router();
var func = require('./commonFunction');
var sendResponse = require('./sendResponse');
var moment = require('moment');
var async = require('async');
var request = require("request");


/* Forgot Password Panel */
router.post('/forgot', function (req, res, next) {
    var email = req.body.email;
    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, email, callback);
        },
            function (callback) {
                func.checkEmailAvailability(res, email, callback);

            }], function (updatePopup) {
            var sql = "SELECT `first_name` FROM `tb_users` WHERE `user_email`=? LIMIT 1";
            connection.query(sql, [email], function (err, user) {
                var userName = user[0].first_name;

                func.sendMailToResetPassword(userName, email, res);
            });


        });
});

/* Reset Password Panel */
router.post('/reset', function (req, res, next) {
    var link = req.body.link;
    var newPassword = req.body.newpass;
    var checkData = [link, newPassword];

    var sql = "SELECT `reset_password_status` FROM `tb_users` WHERE `temporary_link`=? LIMIT 1 "

    connection.query(sql, [link], function (err, result) {
        if (result.length == 0) {
            sendResponse.wrongLink(res);
        }
        else if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (result[0].reset_password_status == 1) {
            sendResponse.linkAlreadyUsed(res);
        }
        else {
            async.waterfall([
                function (callback) {

                    func.checkBlankWithCallback(res, checkData, callback);
                }], function (updatePopup) {
                var sql = "SELECT `user_id` FROM `tb_users` WHERE `temporary_link`=? LIMIT 1"
                connection.query(sql, [link], function (err, result) {
                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var md5 = require('MD5');
                        var hash = md5(newPassword);
                        var sql = "UPDATE `tb_users` SET `temporary_link`=? , `reset_password_status`=? , `password`=? WHERE `user_id`=? LIMIT 1"
                        connection.query(sql, [link, 1, hash, result[0].user_id], function (err, result) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                sendResponse.resetSuccessful(res);
                            }
                        })

                    }


                });
            });
        }
    });

});

module.exports = router;