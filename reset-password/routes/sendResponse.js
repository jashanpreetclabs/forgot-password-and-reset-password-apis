/**
 * Created by JASHAN on 3/9/2015.
 */

var constant = require('./constant');

exports.parameterMissingError = function (res) {

    var errResponse = {
        status: constant.responseStatus.PARAMETER_MISSING,
        message: constant.responseMessage.PARAMETER_MISSING,
        data: {}
    }
    sendData(errResponse, res);
};

exports.emailNotRegistered = function (res) {

    var errResponse = {
        status: constant.responseStatus.EMAIL_NOT_REGISTERED,
        message: constant.responseMessage.EMAIL_NOT_REGISTERED,
        data: {}
    }
    sendData(errResponse, res);
};

exports.sendLog = function (res) {

    var successResponse = {
        status: constant.responseStatus.RESET_EMAIL_SEND,
        message: constant.responseMessage.RESET_EMAIL_SEND,
        data: {}
    }
    sendData(successResponse, res);
};

exports.somethingWentWrongError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.ERROR_IN_EXECUTION,
        data: {}
    }
    sendData(errResponse, res);
};

exports.linkAlreadyUsed = function (res) {

    var errResponse = {
        status: constant.responseStatus.LINK_USED,
        message: constant.responseMessage.LINK_USED,
        data: {}
    }
    sendData(errResponse, res);
};
exports.wrongLink = function (res) {

    var errResponse = {
        status: constant.responseStatus.WRONG_LINK,
        message: constant.responseMessage.WRONG_LINK,
        data: {}
    }
    sendData(errResponse, res);
};


exports.resetSuccessful = function (res) {

    var successResponse = {
        status: constant.responseStatus.RESET_SUCCESSFUL,
        message: constant.responseMessage.RESET_SUCCESSFUL,
        data: {}
    }
    sendData(successResponse, res);
};

function sendData(data, res) {
    res.type('json');
    res.jsonp(data);
}