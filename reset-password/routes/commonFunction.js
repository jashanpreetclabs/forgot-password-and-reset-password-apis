/**
 * Created by JASHAN on 3/9/2015.
 */
var sendResponse = require('./sendResponse');
var config = require('config');
var generatePassword = require('password-generator');
var md5 = require('MD5');


exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}
exports.checkEmailAvailability = function (res, email, callback) {

    var sql = "SELECT `user_id` FROM `tb_users` WHERE `user_email`=? limit 1";
    connection.query(sql, [email], function (err, response) {

        if (err) {
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            callback(null);
        }
        else {
            sendResponse.emailNotRegistered(res);
        }
    });
};

function sendEmail(receiverMailId, message, subject, callback) {

    var nodemailer = require('nodemailer');
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: config.get('emailSettings').email,
            pass: config.get('emailSettings').password
        }
    });

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailSettings').email,
        to: receiverMailId,
        subject: subject,
        text: message
        //html: "<b>Hello world ?</b>" // html body
    };

// send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function (error, response) {

        if (error) {
            console.log(mailOptions);
            console.log(error);
            return callback(0);

        }
        else {
            return callback(1);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        //smtpTransport.close(); // shut down the connection pool, no more messages
    });

};

exports.sendMailToResetPassword = function (userName, email, res) {
    var password = generatePassword(6, false);
    var encrypted_password = md5(password);
    var toEmail = email;
    var sub = "Password Reset";
    var html = "Information to reset your password";
    html += "Dear ' + userName + '";
    html += "Thank you for contacting us. Click the button below to reset your password. This link is valid for one use only.";
    html += "'+encrypted_password+'";
    html += 'Please do not reply to this email. Emails sent to this address will not be answered.';
    html += 'Copyright &copy; 2015 Jashanpreet Singh, LLC. All rights reserved.';
    sendEmail(toEmail, html, sub, function (result) {
        if (result == 1) {
            var sql = "UPDATE `tb_users` SET `temporary_link`=? , `reset_password_status`=?  WHERE `user_email`=? LIMIT 1"
            connection.query(sql, [encrypted_password, 0, toEmail], function (err, result) {
                if (err) {

                    sendResponse.somethingWentWrongError(err);
                }
                else {

                    sendResponse.sendLog(res);
                }
            })

        }
        else {
            console.log("hello");
            sendResponse.somethingWentWrongError(res);
        }
    });
}