/**
 * Created by JASHAN on 3/9/2015.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: false
    });
}

exports.responseStatus = {};

define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "ERROR_IN_EXECUTION", 101);
define(exports.responseStatus, "RESET_SUCCESSFUL", 102);
define(exports.responseStatus, "EMAIL_NOT_REGISTERED", 103);
define(exports.responseStatus, "RESET_EMAIL_SEND", 104);
define(exports.responseStatus, "LINK_USED", 105);
define(exports.responseStatus, "WRONG_LINK", 106);


exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "ERROR_IN_EXECUTION", "Some error occurred. Please try again.");
define(exports.responseMessage, "EMAIL_NOT_REGISTERED", "Oops your email has not been registered. Please sign up.");
define(exports.responseMessage, "RESET_SUCCESSFUL", "Reset Successful.");
define(exports.responseMessage, "RESET_EMAIL_SEND", "Check your email to change password");
define(exports.responseMessage, "LINK_USED", "Link has Expired.");
define(exports.responseMessage, "WRONG_LINK", "Link does not exists.");

